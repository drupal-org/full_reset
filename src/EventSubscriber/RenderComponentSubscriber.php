<?php

namespace Drupal\full_reset\EventSubscriber;

use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RenderComponentSubscriber.
 */
class RenderComponentSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender'];
    return $events;
  }

  /**
   * Resets the theming of the rendered section component.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    if ($event->inPreview()) {
      return;
    }
    $build = $event->getBuild();
    if (isset($build['#theme'])) {
      $settings = $event->getComponent()->getThirdPartySettings('full_reset');
      if (isset($settings['reset']) && $settings['reset']) {
        $build['#theme'] = NULL;
        $event->setBuild($build);
      }
    }
  }

}
