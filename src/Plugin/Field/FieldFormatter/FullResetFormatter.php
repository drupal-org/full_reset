<?php

namespace Drupal\full_reset\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'full_reset' field formatter.
 *
 * The annotation does not specify any field types. Instead, all field types are
 * added in this module's hook_field_formatter_info_alter() implementation.
 *
 * @FieldFormatter(
 *   id = "full_reset",
 *   label = @Translation("Full Reset"),
 *   field_types = {}
 * )
 */
class FullResetFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = $item->view();
    }
    $elements['#theme'] = NULL;
    return $elements;
  }

}
