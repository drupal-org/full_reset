# Full Reset

Adds options to fully remove theming from fields and Layout Builder blocks.

This can be especially useful when using layout definitions and Layout Builder in component-based theming.

Currently requires a core patch from [this issue](https://www.drupal.org/project/drupal/issues/3015152) to work with Layout Builder blocks.
